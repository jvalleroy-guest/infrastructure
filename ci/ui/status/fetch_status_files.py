#!/usr/bin/env python3
# SPDX-License-Identifier: AGPL-3.0-or-later
"""
Download status files for local development.
"""

import json
import pathlib
import subprocess

base_path = 'https://ci.freedombox.org/status/'

pipelines = json.loads(open('pipeline-info.json').read())
args_to_curl = []

for group in pipelines:
    group_name = group['name'].split('-')[-1]
    for pipeline in group['pipelines']:
        status_file = f'{pipeline}-{group_name}-status.json'
        pathlib.Path(status_file).unlink(missing_ok=True)
        args_to_curl += ['--remote-name', base_path + status_file]

subprocess.run(['curl', '--parallel', *args_to_curl])
