#!/usr/bin/env bash

# This script should be run as user "admin" on a Debian machine

function clone_repo() {
    mkdir ~/dev
    cd ~/dev || return
    git clone --depth 1 https://salsa.debian.org/freedombox-team/freedombox.git
}

function run_functional_tests() {
    export DISTRIBUTION=$1
    cd ~/dev/freedombox/ || return
    ./container up
    ./container run-tests --pytest-args -v --include-functional --splinter-screenshot-dir="screenshots-$DISTRIBUTION" | tee "functional-tests-$DISTRIBUTION.log"
    # --template=html1/index.html --report="functional-tests-$DISTRIBUTION.html"
}

clone_repo

# TODO Find a better solution based on systemd

pids=""

for dist in stable testing unstable; do
   run_functional_tests $dist &
   pids="$pids $!"
done

wait $pids
