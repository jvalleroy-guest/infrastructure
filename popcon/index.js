"use strict";

// SPDX-License-Identifier: AGPL-3.0-or-later

const compareButton = document.getElementById('compare-button');
compareButton.addEventListener('click', function () {
  window.location.href = 'compare.html'
});

function makeImg(pkg) {
  let img = document.createElement('img');
  img.classList = ["grid-item"];
  img.src = `https://qa.debian.org/cgi-bin/popcon-png?packages=${pkg}&show_installed=1&date_fmt=%25Y-%25m`;
  img.alt = `${pkg}-popcon-graph`;
  return img;
}

function loadGraphs(data) {
  let row = document.getElementById('graphs');
  let promises = data.map(element => {
    return new Promise((resolve, reject) => {
      let img = makeImg(element);
      img.onload = () => resolve(img);
      img.onerror = reject;
      row.append(img);
    });
  });
  Promise.all(promises)
    .then(() => console.log('All images loaded successfully'))
    .catch((error) => console.error('Error loading images:', error));
}

fetch('/packages.json')
  .then(response => response.json())
  .then(data => loadGraphs(data));
