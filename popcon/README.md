# FreedomBox Popularity Contest

This directory has the files required to host a dashboard showing the popularity of FreedomBox apps.  

The live version of the dashboard is at https://popcon.freedombox.org
