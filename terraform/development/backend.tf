terraform {
  backend "s3" {
    bucket         = "freedombox-terraform-state-development"
    region         = "us-east-1"
    key            = "development.json"
    dynamodb_table = "terraform-state-lock"
    encrypt        = true
  }
}
