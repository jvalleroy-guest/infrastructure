# SPDX-License-Identifier: AGPL-3.0-or-later
"""
Read and serve global configuration from a JSON file.
"""

import json
import pathlib

conf = None


def load():
    """Read JSON file from /etc and return a dictionary."""
    global conf
    if conf is not None:
        return

    config_path = pathlib.Path('/etc/buildbot/config.json')
    if not config_path.exists():
        raise RuntimeError(f'Configuration file not found: {config_path}')

    conf = json.loads(config_path.read_bytes())


load()
