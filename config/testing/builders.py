# SPDX-License-Identifier: AGPL-3.0-or-later

from ..common.builders import BuilderConfigFactory, get_build_targets

_DISTRIBUTION = 'testing'
_BUILD_STAMP = 'latest'

_build_targets = get_build_targets(_DISTRIBUTION, _BUILD_STAMP)

builders = [
    BuilderConfigFactory(
        build_target.name, build_target.artifact_name, build_target.image_size,
        distribution='testing',
        extra_release_components=build_target.extra_release_components,
        build_stamp='latest').create() for build_target in _build_targets
]
