# SPDX-License-Identifier: AGPL-3.0-or-later

from buildbot.plugins import steps, util

from .. import conf
from ..common import rsync_common_opts, rsync_url

BUILDER_NAME = 'functional-tests-dev'
_TEST_REPORT = 'functional-tests-dev.html'

_repo_url = conf.conf['repos']['freedombox']['url']
_steps = [
    steps.Git(name='fetch latest freedombox code', repourl=_repo_url,
              mode='full', haltOnFailure=True),
    steps.FileDownload(
        name='Download requirements installation script',
        mastersrc='scripts/functional-tests/install-requirements.sh',
        workerdest='install-requirements.sh'),
    steps.ShellCommand(name='install requirements for functional tests',
                       command=['/bin/bash', 'install-requirements.sh'],
                       env={"DEBIAN_FRONTEND": "noninteractive"}),
    steps.ShellCommand(
        name='run functional tests',
        command=[
            'py.test-3',
            '-v',  # '-n', '2', '--dist', 'loadfile',
            '--include-functional',
            '--template=html1/index.html',
            f'--report={_TEST_REPORT}'
        ],
        timeout=3600),
    steps.ShellCommand(
        name='upload test results', command=['rsync'] + rsync_common_opts() + [
            '--chmod=644', _TEST_REPORT,
            rsync_url(conf.conf['upload']['log_path'])
        ])
]

builders = [
    util.BuilderConfig(name=BUILDER_NAME, workernames=["bilbo"],
                       factory=util.BuildFactory(_steps),
                       env={'FREEDOMBOX_URL': 'https://mordor:4430'})
]
