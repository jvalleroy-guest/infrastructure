# SPDX-License-Identifier: AGPL-3.0-or-later

from buildbot.plugins import schedulers

from . import app_server, functional_tests, plinth
from .builders import builders

# One force scheduler per builder configuration
force_schedulers = [
    schedulers.ForceScheduler(name="force-{}-build".format(builder.name),
                              builderNames=[builder.name])
    for builder in builders
]

virtualbox_scheduler = schedulers.Dependent(
    name='virtualbox-build', builderNames=['virtualbox-amd64-dev'],
    upstream=plinth.periodic_scheduler)

app_server_scheduler = schedulers.Dependent(
    name='app-server-build', builderNames=[app_server.BUILDER_NAME],
    upstream=virtualbox_scheduler)

functional_tests_scheduler = schedulers.Dependent(
    name='functional-tests-build',
    builderNames=[functional_tests.BUILDER_NAME],
    upstream=app_server_scheduler)

dependent_schedulers = [
    virtualbox_scheduler, app_server_scheduler, functional_tests_scheduler
]

# Runs at 1 am on Saturdays
weekly_schedulers = [
    schedulers.Nightly(name='weekly-dev', branch='main', builderNames=[
        'amd64-testing-dev', 'amd64-unstable-dev'
    ], hour=1, minute=0, dayOfWeek=5)  # Monday is 0
]

schedulers = force_schedulers + weekly_schedulers

builders = builders
