# SPDX-License-Identifier: AGPL-3.0-or-later

from ..common.builders import BuilderConfigFactory, get_build_targets

_DISTRIBUTION = 'bookworm'
_BUILD_STAMP = ''  # avoid saying bookworm twice in the name

_build_targets = get_build_targets(_DISTRIBUTION, _BUILD_STAMP)

builders = [
    BuilderConfigFactory(
        build_target.name, build_target.artifact_name, build_target.image_size,
        distribution=_DISTRIBUTION,
        extra_release_components=build_target.extra_release_components,
        build_stamp=_BUILD_STAMP).create() for build_target in _build_targets
]
