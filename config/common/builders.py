# SPDX-License-Identifier: AGPL-3.0-or-later

import math
import os
from dataclasses import dataclass

from buildbot.plugins import steps, util
from buildbot.process import results

from .. import common, conf

WORKER = 'legolas'

# Open trackers used in torrent creation. These are two of the biggest
# trackers and have a relatively stable history.
TRACKERS = [
    'udp://tracker.opentrackr.org:1337/announce',
    'udp://tracker.openbittorrent.com:6969',
]

# Exclusive lock to run only one image build at a time on each worker.
build_lock = util.WorkerLock("image_builds", maxCount=1)

_image_size = conf.conf['images']['default']['size']

_website = 'https://freedombox.org'

_rpi_imager_info = {
    'stable': {
        'name': 'FreedomBox - Stable (for Raspberry Pi 3/4)',
        'description': 'With regular minor updates and major updates every '
                       '2 years'
    },
    'bookworm': {
        'name': 'FreedomBox - Stable (for Raspberry Pi 3/4)',
        'description': 'With regular minor updates and major updates every '
                       '2 years'
    },
    'testing': {
        'name': 'FreedomBox - Testing (for Raspberry Pi 3/4)',
        'description': 'Rolling release with all the latest features'
    },
    'unstable': {
        'name': 'FreedomBox - Unstable (for Raspberry Pi 3/4)',
        'description': 'For FreedomBox developers and testers'
    }
}


@dataclass
class BuildTarget:
    """Build target definition"""
    name: str
    artifact_name: str
    image_size: str = _image_size
    extra_release_components: tuple = ()


class BuilderConfigFactory():
    """Factory to produce a builder configuration."""

    def __init__(self, target, artifact_name, image_size=_image_size,
                 distribution='unstable', extra_release_components=(),
                 build_stamp='dailydebian'):
        self.target = target
        self.artifact_name = artifact_name
        self.image_size = image_size
        self.distribution = distribution
        self.extra_release_components = extra_release_components
        self.build_stamp = build_stamp

    def builder_name(self):
        return self.target and self.target + '-' + self.distribution

    def _get_directory(self):
        directory = self.distribution
        if self.distribution == 'unstable':
            directory = 'nightly'

        return directory

    def _format_artifact_string(self, string):
        return string.format(self.target, self._get_directory())

    def get_artifact_upload_path(self):
        return self._format_artifact_string(
            conf.conf['upload']['hardware_path'])

    def get_artifact_rsync_url(self):
        return self._format_artifact_string(
            common.rsync_url(conf.conf['upload']['hardware_path']))

    def checkout_steps(self):
        repo_url = conf.conf['repos']['freedom_maker']['url']
        return [
            steps.ShellCommand(name='cleanup build directory',
                               command=['sudo', 'rm', '-rf',
                                        'build'], haltOnFailure=True),
            steps.Git(name="checkout source code", repourl=repo_url,
                      mode='full', haltOnFailure=True)
        ]

    def install_prerequisites(self):
        return [
            steps.ShellCommand(
                name='install prerequisites', command=[
                    'sudo', 'DEBIAN_FRONTEND=noninteractive', 'apt-get',
                    'install', '--yes', 'binfmt-support', 'btrfs-progs',
                    'debootstrap', 'debootstrap', 'dosfstools', 'fdisk', 'git',
                    'kpartx', 'parted', 'psmisc', 'python3-cliapp',
                    'qemu-user-static', 'qemu-utils', 'sshpass', 'sudo',
                    'u-boot-tools', 'xz-utils', 'apt-cacher-ng', 'rsync'
                ], haltOnFailure=True),
        ]

    def build_steps(self):
        build_mirror = conf.conf['common']['build_mirror']
        command = [
            'sudo', 'python3', '-B', '-m', 'freedommaker', '--force',
            f'--build-mirror={build_mirror}',
            f'--build-stamp={self.build_stamp}',
            f'--image-size={self.image_size}',
            f'--distribution={self.distribution}', '--build-in-ram'
        ]
        for component in self.extra_release_components:
            command.append(f'--add-release-component={component}')

        command.append(self.target)
        return [
            steps.ShellCommand(name='build FreedomBox image', command=command,
                               haltOnFailure=True, timeout=3600)
        ]

    def _get_web_seed_url(self):
        return (conf.conf['download']['hardware_url'].format(self.target) +
                self._get_directory() + '/' + self.artifact_name)

    def publish_steps(self):
        host = conf.conf['upload']['host']
        user = conf.conf['upload']['user']
        port = conf.conf['upload']['port']
        base_path = conf.conf['upload']['base_path']
        create_folder_script = f'''sftp -P {port} {user}@{host} <<EOF
        mkdir {base_path}/hardware/{self.target}
        mkdir {self.get_artifact_upload_path()}
        EOF
        '''
        piece_length = 19  # 512 KiB, meaning 1000 pieces for 500 MiB file
        return [
            steps.ShellCommand(
                name='change ownership',
                command=['sudo', 'chown', '-R', 'buildbot:buildbot',
                         'build'], alwaysRun=True),
            steps.ShellCommand(
                name="sign the artifact", command=[
                    'gpg', '--batch', '--yes', '--detach-sign',
                    'build/' + self.artifact_name
                ], haltOnFailure=True),
            steps.ShellCommand(name="change permissions",
                               command="chmod -R 644 build/*"),
            steps.ShellCommand(name="create folder if needed",
                               command=create_folder_script,
                               haltOnFailure=True),
            steps.ShellCommand(
                name="upload artifact",
                command=['rsync'] + common.rsync_common_opts() + [
                    '-a', 'build/' + self.artifact_name,
                    self.get_artifact_rsync_url()
                ], timeout=1800, haltOnFailure=True),
            steps.ShellCommand(
                name="upload signature",
                command=['rsync'] + common.rsync_common_opts() + [
                    '-a', 'build/' + self.artifact_name + '.sig',
                    self.get_artifact_rsync_url()
                ]),
            steps.ShellCommand(
                name="create a torrent file", command=[
                    'mktorrent', '--announce', TRACKERS[0], '--announce',
                    TRACKERS[1], '--web-seed',
                    self._get_web_seed_url(), '--output',
                    self.artifact_name + '.torrent', '--piece-length',
                    str(piece_length), 'build/' + self.artifact_name
                ]),
            steps.ShellCommand(
                name="upload torrent file",
                command=['rsync'] + common.rsync_common_opts() + [
                    '-a', self.artifact_name + '.torrent',
                    self.get_artifact_rsync_url()
                ]),
        ]

    def _with_rpi_imager(self):
        return self.target == 'raspberry64'

    def _rpi_imager_steps(self):
        name = _rpi_imager_info[self.distribution]['name']
        description = _rpi_imager_info[self.distribution]['description']
        rsync_url = self.get_artifact_rsync_url()
        rsync_url = rsync_url.rstrip('/').rpartition('/')[0] + '/'

        url = conf.conf['download']['hardware_url'].format(self.target)
        icon_url = url + 'icon.png'
        image_url = url + self._get_directory() + '/' + self.artifact_name
        imager_url = url + 'rpi-imager.json'
        return [
            steps.ShellCommand(name='clean Raspberry Pi Imager JSON',
                               command=['rm', '-f', 'build/rpi-imager.json'],
                               haltOnFailure=False),
            steps.ShellCommand(
                name='fetch Raspberry Pi Imager JSON',
                command=['wget', imager_url, '-O',
                         'build/rpi-imager.json'], decodeRC={
                             0: results.SUCCESS,
                             8: results.SUCCESS
                         }, haltOnFailure=False),
            steps.FileDownload(
                name="download script update_rpi_imager_json.py",
                mastersrc="scripts/update_rpi_imager_json.py",
                workerdest="update_rpi_imager_json.py", haltOnFailure=True),
            steps.ShellCommand(
                name='update Raspberry Pi Imager JSON', command=[
                    'python3', 'update_rpi_imager_json.py',
                    '--image-path=build/' + self.artifact_name,
                    '--oslist-path=build/rpi-imager.json', '--name=' + name,
                    '--description=' + description, f'--icon={icon_url}',
                    f'--url={image_url}', f'--website={_website}'
                ], haltOnFailure=True),
            steps.ShellCommand(name="change permissions",
                               command="chmod -R 644 build/*"),
            steps.ShellCommand(
                name="upload Raspberry Pi Imager JSON",
                command=['rsync'] + common.rsync_common_opts() +
                ['-a', 'build/rpi-imager.json', rsync_url]),
        ]

    def steps(self):
        steps_ = (self.checkout_steps() + self.install_prerequisites() +
                  self.build_steps() + self.publish_steps())
        if self._with_rpi_imager():
            steps_ += self._rpi_imager_steps()

        return steps_

    def create(self):
        return util.BuilderConfig(name=self.builder_name(),
                                  workernames=[WORKER],
                                  factory=util.BuildFactory(self.steps()),
                                  locks=[build_lock.access('exclusive')])


def get_build_targets(distribution, build_stamp):
    """Return a list of common build targets available in all distributions."""
    build_stamp = build_stamp + '_' if build_stamp else ''
    return [
        BuildTarget(
            'a20-olinuxino-lime',
            f'freedombox-{distribution}_{build_stamp}a20-olinuxino-lime-armhf.img.xz'
        ),
        BuildTarget(
            'a20-olinuxino-lime2',
            f'freedombox-{distribution}_{build_stamp}a20-olinuxino-lime2-armhf.img.xz'
        ),
        BuildTarget(
            'a20-olinuxino-micro',
            f'freedombox-{distribution}_{build_stamp}a20-olinuxino-micro-armhf.img.xz'
        ),
        BuildTarget(
            'amd64',
            f'freedombox-{distribution}_{build_stamp}all-amd64.img.xz'),
        BuildTarget(
            'qemu-amd64',
            f'freedombox-{distribution}_{build_stamp}all-amd64.qcow2.xz'),
        BuildTarget(
            'virtualbox-amd64',
            f'freedombox-{distribution}_{build_stamp}all-amd64.vdi.xz'),
        BuildTarget('i386',
                    f'freedombox-{distribution}_{build_stamp}all-i386.img.xz'),
        BuildTarget(
            'qemu-i386',
            f'freedombox-{distribution}_{build_stamp}all-i386.qcow2.xz'),
        BuildTarget('virtualbox-i386',
                    f'freedombox-{distribution}_{build_stamp}all-i386.vdi.xz'),
        BuildTarget(
            'cubieboard2',
            f'freedombox-{distribution}_{build_stamp}cubieboard2-armhf.img.xz'
        ),
        BuildTarget(
            'cubietruck',
            f'freedombox-{distribution}_{build_stamp}cubietruck-armhf.img.xz'),
        BuildTarget(
            'pcduino3',
            f'freedombox-{distribution}_{build_stamp}pcduino3-armhf.img.xz'),
        BuildTarget(
            'raspberry2',
            f'freedombox-{distribution}_{build_stamp}raspberry2-armhf.img.xz'),
        BuildTarget(
            'raspberry3',
            f'freedombox-{distribution}_{build_stamp}raspberry3-armhf.img.xz'),
        BuildTarget(
            'raspberry3-b-plus',
            f'freedombox-{distribution}_{build_stamp}raspberry3-b-plus-armhf.img.xz'
        ),
        BuildTarget(
            'raspberry64',
            f'freedombox-{distribution}_{build_stamp}raspberry64-arm64.img.xz'
        ),
        BuildTarget(
            'beaglebone',
            f'freedombox-{distribution}_{build_stamp}beaglebone-armhf.img.xz'),
        BuildTarget(
            'pine64-plus',
            f'freedombox-{distribution}_{build_stamp}pine64-plus-arm64.img.xz'
        ),
        BuildTarget(
            'banana-pro',
            f'freedombox-{distribution}_{build_stamp}banana-pro-armhf.img.xz'),
        BuildTarget(
            'lamobo-r1',
            f'freedombox-{distribution}_{build_stamp}lamobo-r1-armhf.img.xz'),
        BuildTarget(
            'pine64-lts',
            f'freedombox-{distribution}_{build_stamp}pine64-lts-arm64.img.xz'),
        BuildTarget(
            'orange-pi-zero',
            f'freedombox-{distribution}_{build_stamp}orange-pi-zero-armhf.img.xz'
        ),
        BuildTarget('vagrant',
                    f'freedombox-{distribution}_{build_stamp}all-amd64.box',
                    image_size='12G', extra_release_components=('contrib', )),
        BuildTarget(
            'armhf',
            f'freedombox-{distribution}_{build_stamp}all-armhf.img.xz'),
        BuildTarget(
            'arm64',
            f'freedombox-{distribution}_{build_stamp}all-arm64.img.xz'),
    ]
