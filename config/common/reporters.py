# SPDX-License-Identifier: AGPL-3.0-or-later

import json
import os

from buildbot.process import results
from buildbot.reporters.http import HttpStatusPush
from twisted.internet import defer, utils

from .. import common, conf


class FreedomBoxCIStatusPush(HttpStatusPush):
    name = "FreedomBoxCIStatusPush"

    def get_status(self, result):
        if result in [results.SUCCESS, results.WARNINGS]:
            return 'Passed'

        return 'Failed'

    @defer.inlineCallbacks
    def sendMessage(self, reports):
        report = reports[0]
        build = report['builds'][0]
        if build['complete']:
            status = {
                'pipeline_name': build['builder']['name'],
                'result': self.get_status(report['results']),
                'time': build['complete_at'].timestamp()
            }
            if not os.path.isdir('status'):
                os.mkdir('status')

            status_file_name = 'status/{}-status.json'.format(
                status['pipeline_name'])
            open(status_file_name, 'w').write(json.dumps(status))
            yield self.rsync(status_file_name)

    @defer.inlineCallbacks
    def rsync(self, status_file):
        """Copy the pipeline and result data to upload server."""
        target = common.rsync_url(conf.conf['upload']['status_path'])
        args = common.rsync_common_opts() + [
            '--chmod=644', status_file, target
        ]
        yield utils.getProcessValue('rsync', args=args)


class LogUploader(HttpStatusPush):
    name = 'LogUploader'

    @defer.inlineCallbacks
    def sendMessage(self, reports):
        report = reports[0]
        build = report['builds'][0]
        steps = yield self.master.db.steps.getSteps(buildid=build['buildid'])
        steps = sorted(steps, key=lambda step: step['id'])
        full_log = []
        for step in steps:
            full_log.append('==== Step {} - {}\n'.format(
                step['number'], step['name']))
            full_log.append('==== Started at {}\n'.format(step['started_at']))
            logs = yield self.master.db.logs.getLogs(stepid=step['id'])
            for log_item in logs:
                log_data = yield self.master.db.logs.getLogLines(
                    log_item['id'], 0, log_item['num_lines'])
                full_log.append(log_data)

            full_log.append('==== Completed at {}\n\n'.format(
                step['complete_at']))

        full_log = ''.join(full_log)

        if not os.path.isdir('logs'):
            os.mkdir('logs')

        log_file_name = build['builder']['name'] + '.log'
        log_file_path = os.path.join('logs', log_file_name)

        # Strip buildbot's log stream information
        with open(log_file_path, 'w') as log_file:
            for line in full_log.split('\n'):
                log_file.write(line[1:] + '\n')

        target = common.rsync_url(conf.conf['upload']['log_path'])
        args = (common.rsync_common_opts() +
                ['--chmod=644', log_file_path, target])
        yield utils.getProcessValue('rsync', args=args)
