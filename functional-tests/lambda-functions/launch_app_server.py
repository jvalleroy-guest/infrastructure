# SPDX-License-Identifier: AGPL-3.0-or-later

import json
import time

import boto3

ec2 = boto3.client('ec2')


def get_launch_template_id(launch_template_name):
    response = ec2.describe_launch_templates(
        LaunchTemplateNames=[launch_template_name])
    return response['LaunchTemplates'][0]['LaunchTemplateId']


def get_app_server_ip(instance_id):
    response = ec2.describe_instances(InstanceIds=[instance_id])
    return response['Reservations'][0]['Instances'][0]['PublicIpAddress']


def lambda_handler(event, context):
    launch_template_id = get_launch_template_id(event['launch_template_name'])

    response = ec2.run_instances(
        TagSpecifications=[
            {
                'ResourceType':
                    'instance',
                'Tags': [
                    {
                        'Key': 'salsa:project-id',
                        'Value': event['ci_project_id']
                    },
                    {
                        'Key': 'salsa:build-job-id',
                        'Value': event['build_job_id']
                    },
                    {
                        'Key': 'Name',
                        'Value': event['instance_name']
                    },
                ]
            },
        ], MinCount=1, MaxCount=1, LaunchTemplate={
            'LaunchTemplateId': launch_template_id,
        })

    instance_id = response['Instances'][0]['InstanceId']
    time.sleep(3)  # Allow some time for the instance to come up
    app_server_ip = get_app_server_ip(instance_id)

    return {'instance_id': instance_id, 'app_server_ip': app_server_ip}
